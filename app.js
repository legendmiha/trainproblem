requirejs.config({
    baseUrl: 'lib',
    paths: {
        app: '../app'
    },
    config: {
        'app/main': {
            trainsConfig: [
                ['A', 'A2', 100],
                ['B', 'B1', 250],
                ['C', 'C2', 200]
            ],
            moves: 4
        }
    }
});

requirejs(['app/main']);