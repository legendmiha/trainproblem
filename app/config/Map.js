/**
 * Railway Map.
 *
 * Consists of regular stations and cross stations names
 */
define(function () {
    return {
        'A': ['A1', 'A2', 'A3-C3', 'A4', 'A5-B4', 'A6', 'A7'],
        'B': ['B1', 'B2', 'B3-C4', 'A5-B4', 'B5', 'B6'],
        'C': ['C1', 'C2', 'A3-C3', 'B3-C4', 'C5', 'C6']
    }
});
