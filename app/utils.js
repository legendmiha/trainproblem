define(['app/config/Map'], function (Map) {

    /**
     * Find a next station for a particular train
     *
     * @param {Train} train - train for which to get the next station
     * @returns {string} station name
     * @private
     */
    function _findNextStation(train) {
        let railwayMap = Map[train.getRailway()],
            stationIndex = railwayMap.indexOf(train.getStation());

        let direction = (train.isMovingForward()) ? 1 : -1;
        if(stationIndex === railwayMap.length - 1)
            direction = -1;
        else if (stationIndex === 0)
            direction = 1;

        return railwayMap[stationIndex + direction];
    }


    /**
     * Determines if all of the trains are next to the same cross station by checking the direction and
     * the current station of all trains
     *
     * @param {Train[]} trains
     * @return {boolean}
     */
    function trainsAreComingToTheSameCrossStation (...trains) {
        if(trains.length < 2)
            return true;

        const nextStationOfTheFirstTrain = _findNextStation(trains[0]);
        for(let i = 1; i < trains.length; i++) {
            if(_findNextStation(trains[i]) !== nextStationOfTheFirstTrain)
                return false;
        }

        return true;
    }

    /**
     * Determine which train has more passengers and return it
     *
     * @param {Train[]} trains
     * @returns {Train} train with most number of passengers
     */
    function getTrainWithHigherNumberOfPassengers(...trains) {
        return trains.reduce((highestNumberTrain, currentTrain) => {
            const highestNumOfPassengers = highestNumberTrain.getNumOfPassengers(),
                currentNumOfPassengers = currentTrain.getNumOfPassengers();
            return (currentNumOfPassengers > highestNumOfPassengers) ? currentTrain : highestNumberTrain;
        });
    }

    /**
     * Shows positioning of all trains on the Map
     *
     * @param {Train[]} trains
     */
    function showTrainPositions(...trains) {
        trains.forEach((train) => { console.log(`Train ${train.getRailway()} (${train.getNumOfPassengers()} pass.) - ${train.getStation()}`) });
        console.log("-------------------------------------------------");
    }

    /**
     * Determine if the next station for the particular train is available
     * (i.e. there is no train waiting for another train to move)
     *
     * @param {Train} trainToMove
     * @param {Train[]} trains
     * @returns {boolean}
     */
    function nextStationIsAvailable(trainToMove, ...trains) {
        const nextStationOfTheTrain = _findNextStation(trainToMove);
        let nextStationIsAvailable = true;
        trains.forEach((train) => {
            if(train.getStation() === nextStationOfTheTrain)
                nextStationIsAvailable = false;
        });
        return nextStationIsAvailable;
    }

    /**
     * Moves train with the most passengers and remaining train if the next
     * station is available
     *
     * @param {Train} train1 - first train approaching cross station
     * @param {Train} train2 - second train approaching the same cross station
     * @param {Train} train3 - remaining train
     */
    function moveTrainsWithBlockingIssue(train1, train2, train3) {
        getTrainWithHigherNumberOfPassengers(train1, train2).move();
        if(nextStationIsAvailable(train3, train1, train2))
            train3.move();
    }

    return {
        trainsAreComingToTheSameCrossStation,
        getTrainWithHigherNumberOfPassengers,
        showTrainPositions,
        nextStationIsAvailable,
        moveTrainsWithBlockingIssue
    }

});