define(['app/config/Map'], function (Map) {

    /**
     * Class representing a Train
     */
    return class Train {
        /**
         * Create a Train
         * @param {string} railway - railway for this train
         * @param {string} station - current station where train stopped
         * @param {number} numOfPeople - number of people inside a train
         * @param {boolean} [movesForward=true] - determines where train is going (forward of backward)
         */
        constructor(railway, station, numOfPeople, movesForward) {
            this.railway = railway;
            this.station = station;
            this.numOfPeople = numOfPeople;
            this.movesForward = movesForward || true;
        }

        /**
         * Moves train to the next station
         * @void
         */
        move() {
            let railwayMap = Map[this.railway],
                stationIndex = railwayMap.indexOf(this.getStation());

            if(stationIndex === 0)
                this.movesForward = true;
            else if(stationIndex === railwayMap.length - 1)
                this.movesForward = false;

            let direction = (this.isMovingForward()) ? 1 : -1;
            this.station = railwayMap[stationIndex + direction];
        }

        /**
         * Get the railway for this train
         * @returns {string} railway name
         */
        getRailway() {
            return this.railway;
        }

        /**
         * Get the current station where train stopped
         * @returns {string} station name
         */
        getStation() {
            return this.station;
        }

        /**
         * Get the number of passengers
         * @returns {number} number of passengers
         */
        getNumOfPassengers() {
            return this.numOfPeople;
        }

        /**
         * Get if train moves forward
         * @returns {boolean}
         */
        isMovingForward() {
            return this.movesForward;
        }
    };
});