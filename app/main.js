define(['app/Train', 'app/utils', 'module'], function (Train, utils, module) {

    (   /**
         * Init self-invoking function
         *
         * @param {object} train1Init - starting station, railway and number of passengers for the first train
         * @param {object} train2Init - starting station, railway and number of passengers for the second train
         * @param {object} train3Init - starting station, railway and number of passengers for the third train
         */
            function(train1Init, train2Init, train3Init) {

            const train1 = new Train(...train1Init),
                train2 = new Train(...train2Init),
                train3 = new Train(...train3Init);

            for(let i = 0; i < module.config().moves; i++) {

                utils.showTrainPositions(train1, train2, train3);

                if (utils.trainsAreComingToTheSameCrossStation(train1, train2, train3)) {
                    utils.getTrainWithHigherNumberOfPassengers(train1, train2, train3).move();
                    continue;
                }
                if (utils.trainsAreComingToTheSameCrossStation(train1, train2)) {
                    utils.moveTrainsWithBlockingIssue(train1, train2, train3);
                    continue;
                }
                if (utils.trainsAreComingToTheSameCrossStation(train1, train3)) {
                    utils.moveTrainsWithBlockingIssue(train1, train3, train2);
                    continue;
                }
                if (utils.trainsAreComingToTheSameCrossStation(train2, train3)) {
                    utils.moveTrainsWithBlockingIssue(train2, train3, train1);
                    continue;
                }

                train1.move();
                train2.move();
                train3.move();
            }

            utils.showTrainPositions(train1, train2, train3);
        }

    )(...module.config().trainsConfig);

});